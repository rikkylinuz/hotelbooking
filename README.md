<h2>Hotel Booking</h2>
A hotel booking system. Guests can book hotel rooms. Managers are responsible for coordinating the booking. Admin manages the system.<br>

There are 5 Hotels all located in Arlington: Mavericks, Ranger, Williams, Shard, and Liberty. All hotels have the same number of rooms (100) and layout (4 floors - 1 through 4). Guests may book up to 4 rooms per booking - assume each booking is only for rooms of the same type. Tax is 8.25%.
| room type | weekday price | weekend price | No. of rooms |
| ------ | ------ | ------ | ------ |
| standard | $100 |  $150 | 88 |
| delux | $135 |  $185 | 8 |
| suite | $225 |  $275 | 4 |

<h3>Built with</h3>
Java(Android Studio)<br>
SQLite

<h3>Use cases</h3>

1.	Hotel Manager - this user has the following functions
- 	views listing of reservations
- 	views details of a specific reservations
- 	views available rooms on a given day/time
-   search room
- 	changes a room type
- 	changes a room rate
- 	makes a room unavailable
- 	updates own profile
- 	does not register - profile is already captured in the system
2.	Guest
- 	registers
- 	search for room type, number of rooms, number of adults/children, check-in and out date
- 	reserves and pays for a room (credit card only - Mastercard, Visa, Discover)
- 	view/modify/cancel reserved rooms
- 	updates own profile
3.	Admin
- 	cannot update but can view their own profile
- 	does not register - profile is already captured in the system
- 	edits guest profile
- 	remove a guest or manager from the system (user must re-register)
-   add/delete new user





