package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

public class AdminViewProfileActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private EditText firstname;
    private EditText lastname;
    private EditText phone;
    private EditText email;
    private EditText street;
    private EditText city;
    private EditText state;
    private EditText zipcode;
    private EditText country;
    private EditText gender;
    private EditText role;
    private DatabaseHelper dbHelper;
    private User user;

    private Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_profile_activity);
        setTitle("View Profile");
        username = findViewById(R.id.userNameViewProfileET);
        password = findViewById(R.id.passwordViewProfileET);
        firstname = findViewById(R.id.firstNameViewProfileET);
        lastname = findViewById(R.id.lastNameViewProfileET);
        phone = findViewById(R.id.phoneViewProfileET);
        email = findViewById(R.id.emailViewProfileET);
        street = findViewById(R.id.streetAddressViewProfileET);
        city = findViewById(R.id.cityViewProfileET);
        state = findViewById(R.id.stateViewProfileET);
        zipcode = findViewById(R.id.zipcodeViewProfileET);
        country = findViewById(R.id.countryViewProfileET);
        gender = findViewById(R.id.genderViewProfileET);
        role = findViewById(R.id.roleViewProfileET);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        Intent i = getIntent();
        user = (User) i.getSerializableExtra("userDetails");

        setValues();

        logoutBtn = findViewById(R.id.logoutVP);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminViewProfileActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void setValues() {
        username.setText(user.getUsername());
        password.setText(user.getPassword());
        firstname.setText(user.getFirstname());
        lastname.setText(user.getLastname());
        phone.setText(user.getPhone());
        email.setText(user.getEmail());
        city.setText(user.getCity());
        street.setText(user.getStreetNo());
        zipcode.setText(user.getZipcode());
        country.setText(user.getCountry());
        gender.setText(user.getGender());
        role.setText(user.getRole());
    }
}
