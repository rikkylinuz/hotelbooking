package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Adapters.AllUsersRecyclerAdapter;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

import java.util.List;

public class ViewAllUsersActivity extends AppCompatActivity {

    private RecyclerView usersView;
    private RecyclerView.Adapter userAdapter;
    private List<User> userList;
    private Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_list_view);
        setTitle("View All Users");

        Intent i = getIntent();
        userList = (List<User>) i.getSerializableExtra("user");
        userAdapter= new AllUsersRecyclerAdapter(this,userList);

        usersView = findViewById(R.id.usersView);

        usersView.setLayoutManager(new LinearLayoutManager(this));
        usersView.setHasFixedSize(true);

        usersView.setAdapter(userAdapter);

        usersView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        logoutBtn = (Button) findViewById(R.id.logoutViewListProfile);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewAllUsersActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
