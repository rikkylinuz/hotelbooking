package com.example.hotelbooking.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

public class ViewSelectedUserActivity extends AppCompatActivity {

    private EditText username;
    private EditText lastname;
    private EditText firstname;
    private EditText phone;
    private EditText email;
    private EditText streetNo;
    private EditText city;
    private EditText state;
    private EditText zipcode;
    private EditText role;
    private Button updateBtn;
    private Button deleteBtn;
    private Button logoutBtn;

    private DatabaseHelper dbHelper;

    private String usernameStr;
    private String lastnameStr;
    private String firstnameStr;
    private String phoneStr;
    private String emailStr;
    private String streetNoStr;
    private String cityStr;
    private String stateStr;
    private String zipcodeStr;
    private String roleStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewguestprofile);

        username = (EditText)findViewById(R.id.usernameViewProfile);
        firstname = (EditText)findViewById(R.id.firstnameViewProfile);
        lastname = (EditText)findViewById(R.id.lastnameViewProfile);
        phone = (EditText)findViewById(R.id.phoneViewProfile);
        email = (EditText)findViewById(R.id.emailViewProfile);
        streetNo = (EditText)findViewById(R.id.street_noViewProfile);
        city = (EditText)findViewById(R.id.cityViewProfile);
        state = (EditText)findViewById(R.id.stateViewProfile);
        zipcode = (EditText)findViewById(R.id.zipcodeViewProfile);
        role = (EditText)findViewById(R.id.roleViewProfile);

        updateBtn = (Button)findViewById(R.id.updateProfileButton);
        deleteBtn = (Button)findViewById(R.id.deleteProfileBtn);
        logoutBtn = (Button)findViewById(R.id.logoutButtonViewProfile);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        User selectedUser = (User)bundle.getSerializable("user");
        setValues(selectedUser);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserDetails(view);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUserFromDB(view);
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences mSharedPreference= getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = mSharedPreference.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(ViewSelectedUserActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void setValues(User user){
        this.username.setText(user.getUsername());
        this.firstname.setText(user.getFirstname());
        this.lastname.setText(user.getLastname());
        this.phone.setText(user.getPhone());
        this.email.setText(user.getEmail());
        this.streetNo.setText(user.getStreetNo());
        this.city.setText(user.getCity());
        this.state.setText(user.getState());
        this.zipcode.setText(user.getZipcode());
        this.role.setText(user.getRole());
    }

    private void updateUserDetails(View view){
        this.usernameStr = username.getText().toString();
        this.firstnameStr = firstname.getText().toString();
        this.lastnameStr = lastname.getText().toString();
        this.phoneStr = phone.getText().toString();
        this.emailStr = email.getText().toString();
        this.streetNoStr = streetNo.getText().toString();
        this.cityStr = city.getText().toString();
        this.stateStr = state.getText().toString();
        this.zipcodeStr = zipcode.getText().toString();
        this.roleStr = role.getText().toString();

        if(validation().equals("success")){
            String newUserName = usernameStr;
            String newUserFirstname = firstnameStr;
            String newUserLastname = lastnameStr;
            String newUserRole = roleStr;
            String newUserPhone = phoneStr;
            String newUserEmail = emailStr;
            String newUserStreetNo = streetNoStr;
            String newUserCity = cityStr;
            String newUserState = stateStr;
            String newUserZipcode = zipcodeStr;
            int output = dbHelper.updateUser(newUserName, newUserFirstname, newUserLastname, newUserRole, newUserPhone,
                    newUserEmail, newUserStreetNo, newUserCity, newUserState,newUserZipcode);
            if(output > 0){
                Toast.makeText(ViewSelectedUserActivity.this, "Updated successfully.", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(ViewSelectedUserActivity.this, validation()+" is not valid", Toast.LENGTH_SHORT).show();
        }
    }

    private String validation() {
        if(username.getText().toString().isEmpty())
            return "Username";
        else if (firstname.getText().toString().isEmpty() || !firstname.getText().toString().matches("^[a-zA-z]*$"))
            return "Firstname";
        else if (lastname.getText().toString().isEmpty() || !lastname.getText().toString().matches("^[a-zA-z]*$"))
            return "Lastname";
        else if (!phone.getText().toString().matches("[0-9]+") || phone.getText().toString().length()!=10)
            return "Phone";
        else if (email.getText().toString().isEmpty()|| !email.getText().toString().contains("@"))
            return "Email";
        else if (streetNo.getText().toString().isEmpty() || streetNo.getText().toString().length()<5)
            return "Street Address";
        else if (city.getText().toString().isEmpty() || city.getText().toString().length()<3)
            return "City";
        else if (state.getText().toString().isEmpty() || state.getText().toString().length()!=2)
            return "State";
        else if (zipcode.getText().toString().isEmpty() || !zipcode.getText().toString().matches("[0-9]+") ||zipcode.getText().toString().length()!=5)
            return "Zip Code";
        return "success";
    }

    private void deleteUserFromDB(View view){
        String uname = username.getText().toString();
        int output = dbHelper.deleteUser(uname);
        if(output > 0){
            Toast.makeText(ViewSelectedUserActivity.this, "Deleted successfully.", Toast.LENGTH_SHORT).show();
        }
        Intent i = new Intent(ViewSelectedUserActivity.this, AdminHomeScreenActivity.class);
        startActivity(i);
    }
}
