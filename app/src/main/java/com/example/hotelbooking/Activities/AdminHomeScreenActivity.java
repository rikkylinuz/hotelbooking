package com.example.hotelbooking.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

import java.io.Serializable;
import java.util.List;

public class AdminHomeScreenActivity extends AppCompatActivity {

    private EditText userLastname;
    private Button searchUserBtn;
    private Button viewAllProfileBtn;
    private Button adduserbtn;
    private Button viewProfile;
    private String uname;
    private DatabaseHelper dbHelper;

    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_activity);
        setTitle("Admin Homescreen");
        pref = getSharedPreferences("user_details",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("user_status","logged_in");
        editor.commit();

        uname = pref.getString("username", null);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        userLastname = findViewById(R.id.userLastname);
        searchUserBtn = (Button) findViewById(R.id.btnSearchuser);
        searchUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchUser(view);
            }
        });

        viewAllProfileBtn = (Button) findViewById(R.id.Viewallprofiles);
        viewAllProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewAllProfile(view);
            }
        });

        adduserbtn = (Button) findViewById(R.id.btnAddprofile);
        adduserbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adduser(view);
            }
        });

        viewProfile = (Button) findViewById(R.id.btnViewprofile);
        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewProfile(view);
            }
        });


        Button logoutBtn = (Button) findViewById(R.id.adminLogout);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                editor.remove("user_status");
                editor.clear();
                editor.commit();
                Intent intent = new Intent(AdminHomeScreenActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void searchUser(View view) {
        List<User> userList;
        if(!userLastname.getText().toString().isEmpty()){
            userList = dbHelper.getUserBasedOnLastName(userLastname.getText().toString());

            if(userList.size() == 0){
                Toast.makeText(AdminHomeScreenActivity.this, " No Results found for the user name.", Toast.LENGTH_SHORT).show();
            } else{
                Intent intent = new Intent(AdminHomeScreenActivity.this, ViewUsersActivity.class);
                intent.putExtra("user", (Serializable) userList);
                startActivity(intent);
            }
        }
    }

    private void viewAllProfile(View v){
        List<User> allUsersList;
        allUsersList = dbHelper.getAllUser();
        if(allUsersList.size() == 0){
            Toast.makeText(AdminHomeScreenActivity.this, "No Users found in the database.", Toast.LENGTH_SHORT).show();
        } else{
            Intent intent = new Intent(AdminHomeScreenActivity.this, ViewAllUsersActivity.class);

            intent.putExtra("user", (Serializable) allUsersList);
            startActivity(intent);
        }
    }

    private void adduser(View view) {
        Intent intent = new Intent(AdminHomeScreenActivity.this, AddUserScreenActivity.class);
        startActivity(intent);
    }

    private void viewProfile(View view) {
        User user = dbHelper.getUserBasedOnUserName(uname);
        if(user != null){
            Intent intent = new Intent(AdminHomeScreenActivity.this, AdminViewProfileActivity.class);
            intent.putExtra("userDetails", (Serializable) user);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(),"No user details found in the database.", Toast.LENGTH_SHORT).show();
        }


    }

}
