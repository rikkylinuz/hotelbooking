package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Room;
import com.example.hotelbooking.R;

public class ViewSelectedRoomActivity extends AppCompatActivity {

    private EditText hotelName;
    private EditText roomType;
    private EditText startDate;
    private EditText roomRate;
    private EditText roomNumber;
    private EditText roomOccupiedStatus;
    private EditText roomAvailablityStatus;

    private Button updateBtn;

    private Button logoutBtn;
    Room selectedRoom;

    private DatabaseHelper dbHelper;
    private String roomTypeStr;
    private String roomRateStr;
    private String roomOccupiedStatusStr;
    private String roomAvailabilityStatusStr;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_selected_room);

        hotelName = findViewById(R.id.hotelNameET);
        roomType = findViewById(R.id.roomTypeET);
        startDate = findViewById(R.id.startDateET);
        roomNumber = findViewById(R.id.roomNumberET);
        roomRate = findViewById(R.id.roomRateET);
        roomOccupiedStatus = findViewById(R.id.roomOccupiedET);
        roomAvailablityStatus = findViewById(R.id.roomAvailabilityET);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        selectedRoom = (Room)bundle.getSerializable("selectedroom");
        setValues(selectedRoom);


        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        updateBtn = findViewById(R.id.modifySelected);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifySelectedRoom(view);
            }
        });


        logoutBtn = (Button) findViewById(R.id.LogoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewSelectedRoomActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    private void setValues(Room room) {
        hotelName.setText(room.getHotelName());
        roomType.setText(room.getRoomType());
        roomRate.setText(room.getRoomRate());
        startDate.setText(room.getSearchedDate());
        roomNumber.setText(room.getRoomNumber());
        roomOccupiedStatus.setText(room.getRoomOccupiedStatus());
        roomAvailablityStatus.setText(room.getRoomAvailabilityStatus());
    }

    private void modifySelectedRoom(View view) {
        roomTypeStr = roomType.getText().toString();
        roomRateStr = roomRate.getText().toString();
        roomOccupiedStatusStr = roomOccupiedStatus.getText().toString();
        roomAvailabilityStatusStr = roomAvailablityStatus.getText().toString();

        if(validateInputs().equals("success")){
            int output = dbHelper.updateRoom(roomTypeStr, roomRateStr, roomOccupiedStatusStr, roomAvailabilityStatusStr, roomNumber.getText().toString());
            if(output > 0){
                Toast.makeText(ViewSelectedRoomActivity.this, "Updated successfully.", Toast.LENGTH_SHORT).show();
            }
        } else{
            Toast.makeText(ViewSelectedRoomActivity.this, validateInputs()+" is not valid", Toast.LENGTH_SHORT).show();
        }
    }

    private String validateInputs(){
        if(!roomTypeStr.equalsIgnoreCase("standard") && !roomTypeStr.equalsIgnoreCase("suite")
                && !roomTypeStr.equalsIgnoreCase("delux")) {
            return "RoomType";
        }
        if(!roomRateStr.matches("[0-9]+(\\.[0-9]+)?")) {
            return "RoomRate(valid value: 150.00)";
        }
        if(!roomOccupiedStatusStr.equalsIgnoreCase("not-occupied") &&
                !roomOccupiedStatusStr.equalsIgnoreCase("occupied")) {
            return "Room Occupied Status(Valid values: not-occupied, occupied)";
        }
        if(!roomAvailabilityStatusStr.equalsIgnoreCase("yes") &&
                !roomAvailabilityStatusStr.equalsIgnoreCase("no")) {
            return "Room Availability Status(Valid values: yes, no)";
        }
        return "success";
    }
}
