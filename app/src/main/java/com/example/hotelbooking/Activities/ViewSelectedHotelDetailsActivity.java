package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Hotel;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.R;

public class ViewSelectedHotelDetailsActivity extends AppCompatActivity {

    private TextView address;
    private TextView hotelName;
    private TextView roomType;
    private TextView checkInDate;
    private TextView availableRooms;
    private TextView duration;
    private TextView roomRate;
    private Spinner noOfRooms;
    private Spinner noOfPeople;
    private Button requestReservation;
    private Button logoutBtn;
    Hotel selectedHotel;


    private DatabaseHelper dbHelper;

    private String uname;
    SharedPreferences pref;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_selected_hotel_room);

        pref = getSharedPreferences("user_details",MODE_PRIVATE);
        uname = pref.getString("username", null);

        address = findViewById(R.id.Address);
        hotelName = findViewById(R.id.hotelName);
        availableRooms = findViewById(R.id.availableRoom);
        roomType = findViewById(R.id.roomType);
        checkInDate = findViewById(R.id.checkInDate);
        duration = findViewById(R.id.duration);
        roomRate = findViewById(R.id.roomNumber);

        noOfRooms = (Spinner)findViewById(R.id.NoOfRooms);
        ArrayAdapter<String> noOfRoomsAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.noOfRooms));
        noOfRoomsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        noOfRooms.setAdapter(noOfRoomsAdapter);
        noOfRooms.setSelection(0);

        noOfPeople = (Spinner)findViewById(R.id.noOfPeople);
        ArrayAdapter<String> noOfPeopleAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.noOfRooms));
        noOfPeopleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        noOfPeople.setAdapter(noOfPeopleAdapter);
        noOfPeople.setSelection(1);


        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        selectedHotel = (Hotel)bundle.getSerializable("hotel");
        setValues(selectedHotel);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        requestReservation = findViewById(R.id.reqHotelReservationBtn);
        requestReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestReservation(view);
            }
        });

        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewSelectedHotelDetailsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setValues(Hotel hotel){
        // Update to address after adding the column
        this.address.setText(hotel.getHotelAddress()+","+hotel.getHotelZipcode());
        this.hotelName.setText(hotel.getHotelName());
        this.roomType.setText(hotel.getRoomType());
        this.duration.setText(hotel.getDuration());
        if(hotel.getRoomType().equalsIgnoreCase("standard")) {
            this.availableRooms.setText(hotel.getAvailableStandardRooms());
        } else if(hotel.getRoomType().equalsIgnoreCase("delux")) {
            this.availableRooms.setText(hotel.getAvailableDeluxRooms());
        } else if(hotel.getRoomType().equalsIgnoreCase("suite")) {
            this.availableRooms.setText(hotel.getAvailableSuiteRooms());
        }
        this.checkInDate.setText(hotel.getCheckinDate());
        this.roomRate.setText(hotel.getTotalPrice());

    }

    private void requestReservation(View view) {
        selectedHotel.setNoOfRoomsForUser(noOfRooms.getSelectedItem().toString());
        selectedHotel.setNoOfPeople(noOfPeople.getSelectedItem().toString());
        Double price = ( Double.parseDouble(selectedHotel.getNoOfRoomsForUser()) *Double.parseDouble(selectedHotel.getTotalPrice()) );
        selectedHotel.setTotalPrice( price.toString() );
        Intent intent = new Intent(getApplicationContext(), ConfirmReservationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("selectedhotel", selectedHotel);
        intent.putExtras(bundle);
        startActivity(intent);
    }




}
