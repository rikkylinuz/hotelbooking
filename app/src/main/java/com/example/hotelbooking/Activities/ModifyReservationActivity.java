package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.R;

public class ModifyReservationActivity extends AppCompatActivity {

    private EditText roomType;
    private EditText checkInDate;
    private EditText checkOutDate;
    private EditText noOfRooms;
    private EditText noOfPeople;
    private String reservationIdStr;
    private String roomTypeStr;
    private String checkInDateStr;
    private String checkOutDateStr;
    private String noOfRoomsStr;
    private String noOfPeopleStr;
    private Button updateBtn;
    private DatabaseHelper dbHelper;
    Reservation selectedReservation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_selected_reservation);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        selectedReservation = (Reservation)bundle.getSerializable("reservation");
        reservationIdStr = selectedReservation.getReservationId();

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        roomType = (EditText)findViewById(R.id.roomTypeTV);
        checkInDate = (EditText)findViewById(R.id.checkInTV);
        checkOutDate = (EditText)findViewById(R.id.checkOutTV);
        noOfRooms = (EditText) findViewById(R.id.noOfRoomsTV);
        noOfPeople = (EditText)findViewById(R.id.noOfPeopleTV);

        setValues();

        updateBtn = findViewById(R.id.modifySelectedReservationBtn);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateReservation(view);
            }
        });
    }

    private void setValues(){
        roomType.setText(selectedReservation.getRoomType());
        checkInDate.setText(selectedReservation.getCheckinDate());
        checkOutDate.setText(selectedReservation.getCheckoutDate());
        noOfRooms.setText(selectedReservation.getNumberOfRoom());
        noOfPeople.setText(selectedReservation.getNumberOfPeople());
    }

    private void updateReservation(View view){

        this.roomTypeStr = roomType.getText().toString();
        this.checkInDateStr = checkInDate.getText().toString();
        this.checkOutDateStr = checkOutDate.getText().toString();
        this.noOfRoomsStr = noOfRooms.getText().toString();
        this.noOfPeopleStr = noOfPeople.getText().toString();

        if(val().equals("success")){
            int output = dbHelper.updateReservation(reservationIdStr, roomTypeStr, checkInDateStr, checkOutDateStr,
                    noOfRoomsStr, noOfPeopleStr);
            if(output > 0){
                Toast.makeText(ModifyReservationActivity.this, "Updated successfully.", Toast.LENGTH_SHORT).show();
            }
            Intent i = new Intent(ModifyReservationActivity.this, ViewSelectedReservationActivity.class);

            startActivity(i);
        } else {
            Toast.makeText(ModifyReservationActivity.this, val()+" is not valid", Toast.LENGTH_SHORT).show();
        }

    }

    private String val() {
        if(roomType.getText().toString().isEmpty())
            return "RoomType";
        else if(checkInDate.getText().toString().isEmpty())
            return "CheckInDate";
        else if(checkOutDate.getText().toString().isEmpty())
            return "CheckOutDate";
        else if(noOfRooms.getText().toString().isEmpty())
            return "NoOfRooms";
        else if(noOfPeople.getText().toString().isEmpty())
            return "NoOfPeople";
        else
            return "success";
    }

}
