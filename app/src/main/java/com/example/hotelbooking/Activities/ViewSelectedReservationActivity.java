package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.R;

public class ViewSelectedReservationActivity extends AppCompatActivity {

    private TextView reservationId;
    private TextView hotelName;
    private TextView roomType;
    private TextView checkInDate;
    private TextView checkOutDate;
    private TextView noOfNights;
    private TextView noOfRooms;
    private TextView noOfPeople;
    private TextView totalPrice;
    private Button updateBtn;
    private Button deleteBtn;
    private Button logoutBtn;
    Reservation selectedReservation;

    private DatabaseHelper dbHelper;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_selected_reservation);
        reservationId = (TextView)findViewById(R.id.reservationIdTV);
        hotelName = (TextView)findViewById(R.id.hotelNameTV);
        roomType = (TextView)findViewById(R.id.roomTypeTV);
        checkInDate = (TextView)findViewById(R.id.checkInTV);
        checkOutDate = (TextView)findViewById(R.id.checkOutTV);
        noOfNights = (TextView)findViewById(R.id.noOfNightsTV);
        noOfRooms = (TextView)findViewById(R.id.noOfRoomsTV);
        noOfPeople = (TextView)findViewById(R.id.noOfPeopleTV);
        totalPrice = (TextView)findViewById(R.id.totalPriceTV);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        selectedReservation = (Reservation)bundle.getSerializable("reservation");
        try {
            setValues(selectedReservation);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        updateBtn = findViewById(R.id.modifySelectedReservationBtn);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifyReservation(view);
            }
        });

        deleteBtn = findViewById(R.id.deleteSelectedReservationBtn);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteReservation(view);
            }
        });

        logoutBtn = (Button) findViewById(R.id.logoutViewSelectedReservationBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewSelectedReservationActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setValues(Reservation reservation) throws ParseException {
        this.reservationId.setText(reservation.getReservationId());
        this.hotelName.setText(reservation.getHotelName());
        this.roomType.setText(reservation.getRoomType());
        this.checkInDate.setText(reservation.getCheckinDate());
        this.checkOutDate.setText(reservation.getCheckoutDate());
        this.noOfRooms.setText(reservation.getNumberOfRoom());
        this.noOfPeople.setText(reservation.getNumberOfPeople());
        this.totalPrice.setText(reservation.getPayment());
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Date checkInDate = null;
        try {
            checkInDate = sdf.parse(reservation.getCheckinDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date checkOutDate = null;
        try {
            checkOutDate = sdf.parse(reservation.getCheckoutDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LocalDate checkInLDate = checkInDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate checkOutLDate = checkOutDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long nights = ChronoUnit.DAYS.between(checkInLDate, checkOutLDate);
        this.noOfNights.setText(String.valueOf(nights));
    }

    private void modifyReservation(View v){
        Intent i = new Intent(ViewSelectedReservationActivity.this, ModifyReservationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("reservation", (Reservation)selectedReservation);
        i.putExtras(bundle);
        startActivity(i);

    }

    private void deleteReservation(View v){
        int output = dbHelper.deleteReservation(selectedReservation.getReservationId());
        if(output > 0){
            Toast.makeText(ViewSelectedReservationActivity.this, "Deleted successfully.", Toast.LENGTH_SHORT).show();
        }
        Intent i = new Intent(ViewSelectedReservationActivity.this, GuestHomeScreenActivity.class);
        startActivity(i);
    }

}
