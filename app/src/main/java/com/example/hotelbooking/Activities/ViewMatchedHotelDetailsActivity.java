package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Adapters.MatchedHotelsRecyclerAdapter;
import com.example.hotelbooking.Model.Hotel;
import com.example.hotelbooking.R;

import java.util.List;

public class ViewMatchedHotelDetailsActivity extends AppCompatActivity {

    private RecyclerView hotelView;
    private RecyclerView.Adapter hotelAdapter;
    private Button logoutBtn;
    private List<Hotel> hotelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.matched_hotels_list_view);
        setTitle("View Matched Hotel Rooms");

        Intent i = getIntent();
        hotelList = (List<Hotel>)i.getSerializableExtra("hotelList");

        hotelView = (RecyclerView) findViewById(R.id.hotelsView);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        hotelView.setLayoutManager(layoutManager);
        hotelView.setHasFixedSize(true);

        hotelAdapter = new MatchedHotelsRecyclerAdapter(this, hotelList);
        hotelView.setAdapter(hotelAdapter);
        hotelView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewMatchedHotelDetailsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
