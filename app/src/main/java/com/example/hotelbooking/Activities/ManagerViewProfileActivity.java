package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

public class ManagerViewProfileActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private EditText firstname;
    private EditText lastname;
    private EditText phone;
    private EditText email;
    private EditText street;
    private EditText city;
    private EditText state;
    private EditText zipcode;
    private EditText country;
    private EditText gender;
    private EditText role;
    private EditText ccNumber;
    private EditText ccExpiry;
    private DatabaseHelper dbHelper;
    private User user;
    private Button updateProfileBtn;
    private Button logoutBtn;

    private String usernameStr;
    private String passwordStr;
    private String lastnameStr;
    private String firstnameStr;
    private String phoneStr;
    private String emailStr;
    private String streetNoStr;
    private String cityStr;
    private String stateStr;
    private String zipcodeStr;
    private String roleStr;
    private String genderStr;
    private String countryStr;
    private String creditcardNumberStr;
    private String creditcardEpiryStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_manager_profile_activity);
        setTitle("View Profile");
        username = findViewById(R.id.userNameViewProfileET);
        password = findViewById(R.id.passwordViewProfileET);
        firstname = findViewById(R.id.firstNameViewProfileET);
        lastname = findViewById(R.id.lastNameViewProfileET);
        phone = findViewById(R.id.phoneViewProfileET);
        email = findViewById(R.id.emailViewProfileET);
        street = findViewById(R.id.streetAddressViewProfileET);
        city = findViewById(R.id.cityViewProfileET);
        state = findViewById(R.id.stateViewProfileET);
        zipcode = findViewById(R.id.zipcodeViewProfileET);
        country = findViewById(R.id.countryViewProfileET);
        gender = findViewById(R.id.genderViewProfileET);
        role = findViewById(R.id.roleViewProfileET);
        ccNumber = findViewById(R.id.cardNumberET);
        ccExpiry = findViewById(R.id.cardExpiryET);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        Intent i = getIntent();
        user = (User) i.getSerializableExtra("userDetails");

        setValues();

        updateProfileBtn = findViewById(R.id.updateProfileBtn);
        updateProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile(view);
            }
        });

        logoutBtn = findViewById(R.id.logoutVP);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManagerViewProfileActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void setValues() {
        username.setText(user.getUsername());
        password.setText(user.getPassword());
        firstname.setText(user.getFirstname());
        lastname.setText(user.getLastname());
        phone.setText(user.getPhone());
        email.setText(user.getEmail());
        city.setText(user.getCity());
        street.setText(user.getStreetNo());
        zipcode.setText(user.getZipcode());
        country.setText(user.getCountry());
        gender.setText(user.getGender());
        role.setText(user.getRole());
        state.setText(user.getState());

    }

    private void updateProfile(View view) {
        this.usernameStr = username.getText().toString();
        this.passwordStr = password.getText().toString();
        this.firstnameStr = firstname.getText().toString();
        this.lastnameStr = lastname.getText().toString();
        this.phoneStr = phone.getText().toString();
        this.emailStr = email.getText().toString();
        this.streetNoStr = street.getText().toString();
        this.cityStr = city.getText().toString();
        this.stateStr = state.getText().toString();
        this.zipcodeStr = zipcode.getText().toString();
        this.roleStr = role.getText().toString();
        this.genderStr = gender.getText().toString();
        this.countryStr = country.getText().toString();
        this.creditcardNumberStr = null;
        this.creditcardEpiryStr = null;

        if (validation().equals("success")) {
            int output = dbHelper.updateUserProfile(usernameStr, passwordStr, firstnameStr, lastnameStr,
                    phoneStr, emailStr, streetNoStr, cityStr, stateStr, zipcodeStr, genderStr,
                    countryStr, roleStr, creditcardNumberStr, creditcardEpiryStr);
            if (output > 0) {
                Toast.makeText(ManagerViewProfileActivity.this, "Updated successfully.", Toast.LENGTH_SHORT).show();
            }
        } else{
            Toast.makeText(ManagerViewProfileActivity.this, validation()+" is not valid", Toast.LENGTH_SHORT).show();
        }
    }

    private String validation() {
        if(username.getText().toString().isEmpty())
            return "Username";
        else if (password.getText().toString().isEmpty() || password.getText().toString().length()<=3)
            return "Password";
        else if (firstname.getText().toString().isEmpty() || !firstname.getText().toString().matches("^[a-zA-z]*$"))
            return "Firstname";
        else if (lastname.getText().toString().isEmpty() || !lastname.getText().toString().matches("^[a-zA-z]*$"))
            return "Lastname";
        else if (!phone.getText().toString().matches("[0-9]+") || phone.getText().toString().length()!=10)
            return "Phone";
        else if (email.getText().toString().isEmpty()|| !email.getText().toString().contains("@"))
            return "Email";
        else if (street.getText().toString().isEmpty() || street.getText().toString().length()<5)
            return "Street Address";
        else if (city.getText().toString().isEmpty() || city.getText().toString().length()<3)
            return "City";
        else if (state.getText().toString().isEmpty() || state.getText().toString().length()!=2)
            return "State";
        else if (zipcode.getText().toString().isEmpty() || !zipcode.getText().toString().matches("[0-9]+") ||zipcode.getText().toString().length()!=5)
            return "Zip Code";
        else if (gender.getText().toString().isEmpty() || gender.getText().toString().length()<4)
            return "Gender";
        else if (country.getText().toString().isEmpty() || country.getText().toString().length()<3)
            return "Country";
        return "success";
    }
}
