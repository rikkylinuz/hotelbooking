package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

public class AddUserScreenActivity extends AppCompatActivity {
    private TextView AddUserHeader;
    private EditText aausername;
    private EditText aapassword;
    private EditText aafname;
    private EditText aalname;
    private EditText aaphnnumber;
    private EditText aaemail;
    private EditText aastreetaddress;
    private EditText aacity;
    private EditText aazipcode;
    private EditText aastate;
    private EditText aagender;
    private EditText aacountry;
    private EditText aarole;
    private EditText aahotelid;
    private Button aaadduser;
    private Button logoutBtn;
    SharedPreferences pref;
    private DatabaseHelper dbHelper;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adduser_activity);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        AddUserHeader = (TextView) findViewById(R.id.AddUserHeader);
        aausername = (EditText)findViewById(R.id.aausername);
        aapassword = (EditText)findViewById(R.id.aapassword);
        aafname = (EditText)findViewById(R.id.aafname);
        aalname = (EditText)findViewById(R.id.aalname);
        aaphnnumber = (EditText)findViewById(R.id.aaphnnumber);
        aaemail = (EditText)findViewById(R.id.aaemail);
        aastreetaddress = (EditText)findViewById(R.id.aastreetaddress);
        aacity = (EditText)findViewById(R.id.aacity);
        aazipcode = (EditText)findViewById(R.id.aazipcode);
        aastate = (EditText)findViewById(R.id.aastate);
        aagender = (EditText)findViewById(R.id.aagender);
        aacountry = (EditText)findViewById(R.id.aacountry);
        aarole = (EditText)findViewById(R.id.aarole);
        aahotelid = (EditText)findViewById(R.id.aahotelid);
        aaadduser = (Button)findViewById(R.id.aaadduser);


        aaadduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validation().equals("success")) {
                    storeUserInDB(view);
                    Intent intent = new Intent(getApplicationContext(), AdminHomeScreenActivity.class);
                    startActivity(intent);

                    Toast.makeText(AddUserScreenActivity.this, "User added successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(AddUserScreenActivity.this, validation()+" is not valid", Toast.LENGTH_SHORT).show();
                }

            }
        });

        logoutBtn = (Button) findViewById(R.id.addUserLogoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddUserScreenActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private String validation() {
        //Toast.makeText(AddUserScreenActivity.this, validation()+aausername.getText().toString().isEmpty()+" is not valid", Toast.LENGTH_SHORT).show();
        if(aausername.getText().toString().isEmpty())
            return "Username";
        else if (aapassword.getText().toString().isEmpty() || aapassword.getText().toString().length()<=3)
            return "Password";
        else if (aafname.getText().toString().isEmpty() || !aafname.getText().toString().matches("^[a-zA-z]*$"))
            return "Firstname";
        else if (aalname.getText().toString().isEmpty() || !aalname.getText().toString().matches("^[a-zA-z]*$"))
            return "Lastname";
        else if (!aaphnnumber.getText().toString().matches("[0-9]+") || aaphnnumber.getText().toString().length()!=10)
            return "Phone";
        else if (aaemail.getText().toString().isEmpty()|| !aaemail.getText().toString().contains("@"))
            return "Email";
        else if (aastreetaddress.getText().toString().isEmpty() || aastreetaddress.getText().toString().length()<5)
            return "Street Address";
        else if (aacity.getText().toString().isEmpty() || aacity.getText().toString().length()<3)
            return "City";
        else if (aastate.getText().toString().isEmpty() || aastate.getText().toString().length()!=2)
            return "State";
        else if (aazipcode.getText().toString().isEmpty() || !aazipcode.getText().toString().matches("[0-9]+") ||aazipcode.getText().toString().length()!=5)
            return "Zip Code";
        else if (aagender.getText().toString().isEmpty() || aagender.getText().toString().length()<4)
            return "Gender";
        else if (aacountry.getText().toString().isEmpty() || aacountry.getText().toString().length()<3)
            return "Country";
        else if (aarole.getText().toString().isEmpty())
            return "Role";
        else if (aarole.getText().toString().equalsIgnoreCase("manager") && aahotelid.getText().toString().isEmpty())
            return "Hotelid";
        else if (aarole.getText().toString().equalsIgnoreCase("guest") && !aahotelid.getText().toString().isEmpty())
            return "Hotelid for guest";
        return "success";
    }


    private void storeUserInDB(View view) {
        String newUserName = aausername.getText().toString();
        String newUserPassword = aapassword.getText().toString();
        String newUserFirstname = aafname.getText().toString();
        String newUserLastname = aalname.getText().toString();
        String newUserPhone = aaphnnumber.getText().toString();
        String newUserEmail = aaemail.getText().toString();
        String newUserStreetNo = aastreetaddress.getText().toString();
        String newUserCity = aacity.getText().toString();
        String newUserState = aastate.getText().toString();
        String newUserZipcode = aazipcode.getText().toString();
        String newUserGender = aagender.getText().toString();
        String newUserCountry = aacountry.getText().toString();
        String newUserRole = aarole.getText().toString();
        String newUserCCNumber = null;
        String newUserCCexpiry = null;
        User newUser = new User(newUserName, newUserPassword, newUserFirstname,newUserLastname,newUserRole, newUserPhone, newUserEmail, newUserStreetNo, newUserCity, newUserState, newUserZipcode, newUserGender, newUserCountry, newUserCCNumber, newUserCCexpiry);

        dbHelper.addUser(newUser);

    }

}
