package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.hotelbooking.Adapters.AllReservationsRecyclerAdapter;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ViewAllReservations extends AppCompatActivity {

    private RecyclerView reservationsView;
    private RecyclerView.Adapter reservationsAdapter;
    private List<Reservation> reservationsList;
    private Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.reservations_list_view);
        setTitle("View All Reservations");

        Intent i = getIntent();
        reservationsList = (List<Reservation>) i.getSerializableExtra("reservation");

        reservationsView = (RecyclerView) findViewById(R.id.reservationsView);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        reservationsView.setLayoutManager(layoutManager);
        reservationsView.setHasFixedSize(true);
        reservationsAdapter= new AllReservationsRecyclerAdapter(this,reservationsList);

        reservationsView.setAdapter(reservationsAdapter);
        reservationsView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewAllReservations.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

}
