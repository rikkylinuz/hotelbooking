package com.example.hotelbooking.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Room;
import com.example.hotelbooking.R;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

public class ViewAvailableRoomsActivity extends AppCompatActivity {

    private TextView date;
    private TextView time;
    private Spinner hotelName;
    private int year, month, day, hour, minute;
    private Button dateBtn;
    private Button timeBtn;
    private Button viewAvailableRoomsBtn;
    private DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_available_room_activity);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        date = findViewById(R.id.dateTV);
        time = findViewById(R.id.timeTV);
        hotelName = findViewById(R.id.spinnerHotelName);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        date.setText(month+1 + "-" + day + "-" +year);
        time.setText(hour + ":" + minute);

        ArrayAdapter<String> hotelNameAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.hotelnames));
        hotelNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hotelName.setAdapter(hotelNameAdapter);
        hotelName.setSelection(0);

        dateBtn = findViewById(R.id.dateBtn);
        timeBtn = findViewById(R.id.timeBtn);

        dateBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                pickDate(view);
            }
        });

        timeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickTime(view);
            }
        });

        viewAvailableRoomsBtn = findViewById(R.id.viewAvailableRoomsBtn);
        viewAvailableRoomsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewAvailableRooms(view);
            }
        });

        Button logoutBtn = (Button) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewAvailableRoomsActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pickDate(View v) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                date.setText(m+1 + "-" + d + "-" +y);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void pickTime(View v) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int h, int m) {
                time.setText(h + ":" + m);
            }
        }, hour, minute, true);
        timePickerDialog.show();
    }

    private void viewAvailableRooms(View view) {
        List<Room> roomsList = dbHelper.getAvailableRoomsOnHotelName(hotelName.getSelectedItem().toString().toLowerCase());
        if(!roomsList.isEmpty() && roomsList.size()>0) {
            Intent intent = new Intent(ViewAvailableRoomsActivity.this, ViewAllMatchedRooms.class);
            intent.putExtra("roomsList", (Serializable) roomsList);
            intent.putExtra("searchedDate", date.getText().toString());
            startActivity(intent);
        }
    }
}
