package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Adapters.AllMatchedRoomsRecyclerAdapter;
import com.example.hotelbooking.Model.Room;
import com.example.hotelbooking.R;

import java.util.List;

public class ViewAllMatchedRooms extends AppCompatActivity {

    private RecyclerView roomsView;
    private RecyclerView.Adapter roomsAdapter;
    private List<Room> roomsList;
    private Button logoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_matched_rooms_activity);
        setTitle("View Matched Rooms");

        Intent i = getIntent();
        roomsList = (List<Room>) i.getSerializableExtra("roomsList");
        String date = (String) i.getSerializableExtra("searchedDate");

        roomsView = (RecyclerView) findViewById(R.id.roomsRecyclerview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);

        roomsView.setLayoutManager(layoutManager);
        roomsView.setHasFixedSize(true);
        roomsAdapter= new AllMatchedRoomsRecyclerAdapter(this,roomsList, date);

        roomsView.setAdapter(roomsAdapter);
        roomsView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewAllMatchedRooms.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

}
