package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Hotel;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.R;

public class ConfirmReservationActivity extends AppCompatActivity {

    private TextView hotelname;
    private TextView roomtype;
    private TextView noOfRoom;
    private TextView noOfPeople;
    private TextView noOfNight;
    private TextView startDate;
    private TextView startTime;
    private TextView checkinDate;
    private TextView checkoutDate;
    private TextView payment;
    private Button confirmReservation;
    private Button logoutBtn;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    Hotel hotel;

    private DatabaseHelper dbHelper;

    private EditText pin;
    private Button confirmBTN;
    private Button cancelBTN;
    private Button logoutBTN;
    private String uname;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_reservation_activity);
        setTitle("View Reservation Details");

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        hotel = (Hotel)bundle.getSerializable("selectedhotel");

        pref = getSharedPreferences("user_details",MODE_PRIVATE);
        uname = pref.getString("username", null);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        hotelname = findViewById(R.id.hotelNameTV);
        roomtype = findViewById(R.id.roomTypeTV);
        noOfRoom = findViewById(R.id.NoOfRoomsTV);
        noOfPeople = findViewById(R.id.noOfPeopleTV);
        noOfNight = findViewById(R.id.noOfNightsTV);
        startDate = findViewById(R.id.startDateTV);
        startTime = findViewById(R.id.startTimeTV);
        checkinDate = findViewById(R.id.checkInDateTV);
        checkoutDate = findViewById(R.id.checkoutDateTV);
        payment = findViewById(R.id.paymentTV);

        setValues();

        confirmReservation = findViewById(R.id.confirmReservation);
        confirmReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmReservation(view);
            }
        });

        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmReservationActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setValues() {
        hotelname.setText(hotel.getHotelName());
        roomtype.setText(hotel.getRoomType());
        noOfRoom.setText(hotel.getNoOfRoomsForUser());
        noOfPeople.setText(hotel.getNoOfPeople());
        noOfNight.setText(hotel.getDuration());
        startDate.setText(hotel.getCheckinDate());
        startTime.setText("12:00pm");
        checkoutDate.setText(hotel.getCheckoutDate());
        checkinDate.setText(hotel.getCheckinDate());
        payment.setText(hotel.getTotalPrice());
    }

    private void confirmReservation(View v) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View view = getLayoutInflater().inflate(R.layout.enter_pin_activity,null);
        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();

        pin = view.findViewById(R.id.pinET);
        confirmBTN = view.findViewById(R.id.confirmBtn);
        confirmBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validatePin().equals("success")) {
                    storeReservationInDB();
                    updateNumberOfRooms(hotel);
                    dbHelper.updateRoomStatus(hotel);
                    Intent intent = new Intent(getApplicationContext(), ConfirmReservationActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("selectedhotel", hotel);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    Toast.makeText(ConfirmReservationActivity.this, "Reservation Confirmed.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ConfirmReservationActivity.this, validatePin()+" is not valid", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelBTN = view.findViewById(R.id.cancelBtn);
        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GuestHomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        logoutBTN = view.findViewById(R.id.logoutBtn);
        logoutBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmReservationActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String validatePin() {
        if(pin.getText().toString().isEmpty() || !pin.getText().toString().matches("[0-9]+")){
            return "pin";
        } else {
            return "success";
        }
    }

    private long storeReservationInDB(){
        Reservation r = new Reservation();
        r.setGuestName(uname);
        r.setHotelName(hotel.getHotelName());
        r.setCheckinDate(hotel.getCheckinDate());
        r.setCheckoutDate(hotel.getCheckoutDate());
        r.setNumberOfPeople(hotel.getNoOfPeople());
        r.setNumberOfRoom(hotel.getNoOfRoomsForUser());
        r.setRoomType(hotel.getRoomType());
        r.setPayment(hotel.getTotalPrice());
        r.setPaymentStatus("paid");

        long output = dbHelper.storeReservation(r);
        return output;
    }

    private void updateNumberOfRooms(Hotel h) {
        dbHelper.updateNumberOfRooms(h);

    }
}
