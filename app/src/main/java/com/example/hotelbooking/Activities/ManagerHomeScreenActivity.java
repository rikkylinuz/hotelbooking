package com.example.hotelbooking.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.Model.Room;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ManagerHomeScreenActivity extends AppCompatActivity {
    SharedPreferences pref;
    private Button viewProfileBtn;
    private Button viewReservationsBtn;
    private String uname;
    private DatabaseHelper dbHelper;
    private Button selectDate;
    private Button selectTime;
    private Button searchRoom;
    private EditText dateET;
    private EditText timeET;
    private int year, month, day, hour, minute;
    private Button viewAvailableRoomsBtn;
    private Spinner hotelNameSpinner;
    private EditText roomNumber;
    private String date;
    private Spinner viewListHotelName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_activity);
        setTitle("Manager Homescreen");
        pref = getSharedPreferences("user_details",MODE_PRIVATE);

        uname = pref.getString("username", null);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        selectDate = findViewById(R.id.selectDateBtn);

        dateET = (EditText) findViewById(R.id.dateET);
        dateET.setText(month+1 + "-" + day + "-" +year);
        date = dateET.getText().toString();

        timeET = (EditText) findViewById(R.id.timeET);
        timeET.setText(hour + ":" + minute);

        selectDate = (Button) findViewById(R.id.selectDateBtn);
        selectDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                pickDate(view);
            }
        });

        selectTime = (Button) findViewById(R.id.selectTimeBtn);
        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickTime(view);
            }
        });
        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        viewProfileBtn = (Button) findViewById(R.id.btnViewProfile);
        viewProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewProfile(view);
            }
        });

        viewReservationsBtn = findViewById(R.id.btnListReservations);
        viewReservationsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewReservations(view);
            }
        });

        viewAvailableRoomsBtn = findViewById(R.id.btnViewAvailableRooms);
        viewAvailableRoomsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewAvailableRooms(view);
            }
        });

        hotelNameSpinner = findViewById(R.id.hotelNameSpinner);
        ArrayAdapter<String> hotelNameAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.hotelnames));
        hotelNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hotelNameSpinner.setAdapter(hotelNameAdapter);
        hotelNameSpinner.setSelection(0);

        viewListHotelName = findViewById(R.id.spinnerHotelName);
        ArrayAdapter<String> viewListHotelNameAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.hotelnames));
        viewListHotelNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        viewListHotelName.setAdapter(viewListHotelNameAdapter);
        viewListHotelName.setSelection(0);

        roomNumber = findViewById(R.id.roomNumberET);

        searchRoom = findViewById(R.id.btnSearchRoom);
        searchRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchRoom(view);
            }
        });

        Button logoutBtn = (Button) findViewById(R.id.adminLogout);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(ManagerHomeScreenActivity.this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void viewProfile(View view) {
        User user = dbHelper.getUserBasedOnUserName(uname);
        if(user != null){
            Intent intent = new Intent(ManagerHomeScreenActivity.this, ManagerViewProfileActivity.class);
            intent.putExtra("userDetails", (Serializable) user);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(),"No user details found in the database.", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pickDate(View v) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                dateET.setText(m+1 + "-" + d + "-" +y);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void pickTime(View v) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int h, int m) {
                timeET.setText(h + ":" + m);
            }
        }, hour, minute, true);
        timePickerDialog.show();
    }

    private void viewReservations(View v){
        List<Reservation> reservationList = new ArrayList<>();
        reservationList = dbHelper.getAllReservations(dateET.getText().toString(), viewListHotelName.getSelectedItem().toString().toLowerCase());
        if(reservationList.size() == 0){
            Toast.makeText(getApplicationContext(),"No Reservations found in the database.", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(ManagerHomeScreenActivity.this, ViewAllReservationsManager.class);
            intent.putExtra("reservation", (Serializable) reservationList);
            startActivity(intent);
        }
    }

    private void viewAvailableRooms(View v) {
        Intent intent = new Intent(ManagerHomeScreenActivity.this, ViewAvailableRoomsActivity.class);
        startActivity(intent);
    }

    private void searchRoom(View v){
        if(validate().equals("success")) {
            Room room = dbHelper.getRoomDetailsBasedOnRoomNumber(roomNumber.getText().toString());
            room.setSearchedDate(date);

            Intent intent = new Intent( ManagerHomeScreenActivity.this, ViewSelectedRoomActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("selectedroom", room);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(ManagerHomeScreenActivity.this, validate(), Toast.LENGTH_SHORT).show();
        }
    }

    private String validate(){
        String hName = hotelNameSpinner.getSelectedItem().toString().toLowerCase();
        if(roomNumber.getText().toString().isEmpty()) {
            return "Enter a valid room number";
        }
        Integer rNumber = Integer.parseInt(roomNumber.getText().toString());
        if(!(rNumber >=1 && rNumber <=500) ) {
            return "Not a valid room number. valid[1-500]";
        }
        if(hName.equals("mavericks") && !(rNumber >=1 && rNumber<=100)) {
            return "Valid Room Number for Mavericks is [1-100]";
        } else if(hName.equals("ranger") && !(rNumber >=101 && rNumber<=200)) {
            return "Valid Room Number for Ranger is [101-200]";
        } else if(hName.equals("williams") && !(rNumber >=201 && rNumber<=300)) {
            return "Valid Room Number for Willams is [201-300]";
        } else if(hName.equals("shard") && !(rNumber >=301 && rNumber<=400)) {
            return "Valid Room Number for Shard is [301-400]";
        } else if(hName.equals("liberty") && !(rNumber >=401 && rNumber<=500)) {
            return "Valid Room Number for Liberty is [401-500]";
        } return "success";
    }
}
