package com.example.hotelbooking.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;
import com.example.hotelbooking.Util.Constants;

public class MainActivity extends AppCompatActivity {
    private Button loginBtn;
    private EditText username;
    private EditText password;
    private TextView registerHereLink;
    private Button registerBtn;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private DatabaseHelper dbHelper;
    private EditText usernameRegister;
    private EditText passwordRegister;
    private EditText lastname;
    private EditText firstname;
    private EditText phone;
    private EditText email;
    private EditText streetNo;
    private EditText city;
    private EditText state;
    private EditText zipcode;
    private EditText gender;
    private EditText country;
    private EditText creditCardNumber;
    private EditText creditCardExpiry;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.userNameId);
        password = (EditText) findViewById(R.id.passwordId);
        loginBtn = (Button) findViewById(R.id.msLoginBtn);

        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();

        registerHereLink = findViewById(R.id.registerTextView);
        final String text ="Don't have an account? Register Here";
        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan=new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                registerPopup();
            }
        };

        ss.setSpan(clickableSpan, 23,text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        registerHereLink.setText(ss);
        registerHereLink.setMovementMethod(LinkMovementMethod.getInstance());

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User loggedInUser;

                if (!username.getText().toString().isEmpty() && !password.getText().toString().isEmpty()) {
                    loggedInUser = dbHelper.getUser(username.getText().toString(), password.getText().toString());
                    Intent intent = null;
                    if (loggedInUser != null) {
                        pref = getSharedPreferences("user_details",MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("username",username.getText().toString());
//                        editor.putString("password",password.getText().toString());
                        editor.putString("zipcode",loggedInUser.getZipcode());
                        editor.apply();
                        if (loggedInUser.getRole().equalsIgnoreCase("guest")) {
                            intent = new Intent(getApplicationContext(), GuestHomeScreenActivity.class);
                            intent.putExtra("loggedInUser", loggedInUser);
                        }else if(loggedInUser.getRole().equalsIgnoreCase("admin")){
                            intent = new Intent(getApplicationContext(), AdminHomeScreenActivity.class);
                            intent.putExtra("loggedInUser", loggedInUser);
                        }else if(loggedInUser.getRole().equalsIgnoreCase("manager")){
                            intent = new Intent(getApplicationContext(), ManagerHomeScreenActivity.class);
                            intent.putExtra("loggedInUser", loggedInUser);
                        }
                        startActivity(intent);
                    } else {
                        intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void registerPopup() {
        dialogBuilder = new AlertDialog.Builder(this);
        final View view = getLayoutInflater().inflate(R.layout.register_activity,null);
        dialogBuilder.setView(view);
        dialog = dialogBuilder.create();
        dialog.show();

        usernameRegister = view.findViewById(R.id.usernameViewProfile);
        passwordRegister = view.findViewById(R.id.passwordViewProfile);
        firstname = view.findViewById(R.id.firstnameViewProfile);
        lastname = view.findViewById(R.id.lastnameViewProfile);
        phone = view.findViewById(R.id.phoneViewProfile);
        email = view.findViewById(R.id.emailViewProfile);
        streetNo = view.findViewById(R.id.streetaddress);
        city = view.findViewById(R.id.cityViewProfile);
        state = view.findViewById(R.id.stateViewProfile);
        zipcode = view.findViewById(R.id.zipcodeViewProfile);
        gender = view.findViewById(R.id.gender);
        country = view.findViewById(R.id.country);
        creditCardNumber = view.findViewById(R.id.ccnumber);
        creditCardExpiry = view.findViewById(R.id.ccexpiry);
        registerBtn = view.findViewById(R.id.registerBtn);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validation().equals("success")) {
                    storeUserInDB(view);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, "User registered successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(MainActivity.this, validation()+" is not valid", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private String validation() {
        if(usernameRegister.getText().toString().isEmpty())
            return "Username";
        else if (passwordRegister.getText().toString().isEmpty() || passwordRegister.getText().toString().length()<=3)
            return "Password";
        else if (firstname.getText().toString().isEmpty() || !firstname.getText().toString().matches("^[a-zA-z]*$"))
            return "Firstname";
        else if (lastname.getText().toString().isEmpty() || !lastname.getText().toString().matches("^[a-zA-z]*$"))
            return "Lastname";
        else if (!phone.getText().toString().matches("[0-9]+") || phone.getText().toString().length()!=10)
            return "Phone";
        else if (email.getText().toString().isEmpty()|| !email.getText().toString().contains("@"))
            return "Email";
        else if (streetNo.getText().toString().isEmpty() || streetNo.getText().toString().length()<5)
            return "Street Address";
        else if (city.getText().toString().isEmpty() || city.getText().toString().length()<3)
            return "City";
        else if (state.getText().toString().isEmpty() || state.getText().toString().length()!=2)
            return "State";
        else if (zipcode.getText().toString().isEmpty() || !zipcode.getText().toString().matches("[0-9]+") ||zipcode.getText().toString().length()!=5)
            return "Zip Code";
        else if (gender.getText().toString().isEmpty() || gender.getText().toString().length()<4)
            return "Gender";
        else if (country.getText().toString().isEmpty() || country.getText().toString().length()<3)
            return "Country";
        else if (!creditCardNumber.getText().toString().matches("[0-9]+") || creditCardNumber.getText().toString().length()!=16)
            return "Credit Card Number";
        else if (!creditCardExpiry.getText().toString().matches("^(0?[1-9]|1[012])\\/\\d{2}$"))
            return "Credit Card Expiry";
        return "success";
    }

    private void storeUserInDB(View view){
        String newUserName = usernameRegister.getText().toString();
        String newUserPassword = passwordRegister.getText().toString();
        String newUserFirstname = firstname.getText().toString();
        String newUserLastname = lastname.getText().toString();
        String newUserRole = Constants.GUEST_ROLE;
        String newUserPhone = phone.getText().toString();
        String newUserEmail = email.getText().toString();
        String newUserStreetNo = streetNo.getText().toString();
        String newUserCity = city.getText().toString();
        String newUserState = state.getText().toString();
        String newUserZipcode = zipcode.getText().toString();
        String newUserGender = gender.getText().toString();
        String newUserCountry = country.getText().toString();
        String newUserCCNumber = creditCardNumber.getText().toString();
        String newUserCCExpiry = creditCardExpiry.getText().toString();

        User newUser = new User(newUserName, newUserPassword, newUserFirstname,newUserLastname,newUserRole, newUserPhone, newUserEmail, newUserStreetNo, newUserCity, newUserState, newUserZipcode, newUserGender, newUserCountry, newUserCCNumber, newUserCCExpiry);

        dbHelper.addUser(newUser);
    }

}
