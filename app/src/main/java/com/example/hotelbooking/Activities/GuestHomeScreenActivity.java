package com.example.hotelbooking.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.hotelbooking.Data.DatabaseHelper;
import com.example.hotelbooking.Model.Hotel;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GuestHomeScreenActivity extends AppCompatActivity {

    private EditText dateET;
    private EditText timeET;
    private Button selectDate;
    private Button selectTime;
    private Button viewReservationBtn;
    private int year, month, day, hour, minute;
    private DatabaseHelper dbHelper;
    private String uname;
    private Button viewProfileBtn;
    private Button updateProfileBtn;
    private Spinner roomType;
    private Spinner duration;
    private Spinner minPrice;
    private Spinner maxPrice;
    private Spinner distance;
    private TextView checkInDate;
    private Button checkInBtn;
    private Button searchRoomBtn;
    private String uzipcode;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guest_activity);
        setTitle("Guest Homescreen");
        pref = getSharedPreferences("user_details",MODE_PRIVATE);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        dbHelper = DatabaseHelper.getInstance(getApplicationContext());
        dbHelper.open();
        uname = pref.getString("username", null);
        uzipcode = pref.getString("zipcode", null);
        dateET = (EditText) findViewById(R.id.dateEditText);
        dateET.setText(month+1 + "-" + day + "-" +year);
        selectDate = (Button) findViewById(R.id.selectDateBtn);
        selectDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                pickDate(view);
            }
        });

        checkInDate = (TextView) findViewById(R.id.checkinTV);
        checkInDate.setText(month+1 + "-" + day + "-" +year);
        checkInBtn = (Button) findViewById(R.id.checkInBtn);
        checkInBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                pickCheckInDate(view);
            }
        });

        timeET = (EditText) findViewById(R.id.timeEditText);
        timeET.setText(hour + ":" + minute);
        selectTime = (Button) findViewById(R.id.selectTimeBtn);
        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickTime(view);
            }
        });

        roomType = (Spinner)findViewById(R.id.roomTypeDD);
        ArrayAdapter<String> roomTypeAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.roomtypes));
        roomTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roomType.setAdapter(roomTypeAdapter);
        roomType.setSelection(1);

        duration = (Spinner)findViewById(R.id.durationDD);
        ArrayAdapter<String> durationAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.duration));
        durationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        duration.setAdapter(durationAdapter);
        duration.setSelection(1);

        minPrice = (Spinner)findViewById(R.id.minPriceDD);
        ArrayAdapter<String> minPriceAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.minPrice));
        minPriceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        minPrice.setAdapter(minPriceAdapter);
        minPrice.setSelection(0);

        maxPrice = (Spinner)findViewById(R.id.maxPriceDD);
        ArrayAdapter<String> maxPriceAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.maxPrice));
        maxPriceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        maxPrice.setAdapter(maxPriceAdapter);
        maxPrice.setSelection(1);

        distance = (Spinner)findViewById(R.id.distanceDD);
        ArrayAdapter<String> distanceAdapter = new ArrayAdapter<>(this,R.layout.spinner_view_list_room_type,
                getResources().getStringArray(R.array.distance));
        distanceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        distance.setAdapter(distanceAdapter);
        distance.setSelection(2);

        searchRoomBtn = (Button) findViewById(R.id.searchRoomBtn);
        searchRoomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchRooms(view);
            }
        });

        viewReservationBtn = (Button) findViewById(R.id.guest_view_my_reservations);
        viewReservationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewReservations(view);
            }
        });

        viewProfileBtn = (Button) findViewById(R.id.guest_view_profile_btn);
        viewProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewProfile(view);
            }
        });

        updateProfileBtn = (Button) findViewById(R.id.updateProfileBtn);

        Button logoutBtn = (Button) findViewById(R.id.guest_logout_btn);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = pref.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(GuestHomeScreenActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Logged out successfully.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pickDate(View v) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                dateET.setText(m+1 + "-" + d + "-" +y);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void pickTime(View v) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int h, int m) {
                timeET.setText(h + ":" + m);
            }
        }, hour, minute, true);
        timePickerDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void pickCheckInDate(View v) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                checkInDate.setText(m+1 + "-" + d + "-" +y);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void viewReservations(View v) {
        List<Reservation> reservationList = new ArrayList<>();

        reservationList = dbHelper.getReservations(uname, dateET.getText().toString());
        if(reservationList.size() == 0){
            Toast.makeText(getApplicationContext(),"No Reservations found in the database.", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(GuestHomeScreenActivity.this, ViewAllReservations.class);
            intent.putExtra("reservation", (Serializable) reservationList);
            startActivity(intent);
        }

    }

    private void viewProfile(View view) {
        User user = dbHelper.getUserBasedOnUserName(uname);
        if(user != null){
            Intent intent = new Intent(GuestHomeScreenActivity.this, GuestViewProfileActivity.class);
            intent.putExtra("userDetails", (Serializable) user);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(),"No user details found in the database.", Toast.LENGTH_SHORT).show();
        }
    }


    private void searchRooms(View v){
        if(validateInputs().equals("success")){
            String userZip = getLatitudeLongitude(uzipcode);
            List<Hotel> hotelList = dbHelper.getHotelDetails(minPrice.getSelectedItem().toString(), maxPrice.getSelectedItem().toString(), roomType.getSelectedItem().toString());
            if(!hotelList.isEmpty() && hotelList.size()>0) {
                List<Hotel> matchedHotelList = evaluateList(userZip, hotelList);
                if(!matchedHotelList.isEmpty() && matchedHotelList.size()>0) {

                    // Sort List
                    Map<Integer, Double> cMap = new HashMap<>();
                    for (int i=0; i<matchedHotelList.size(); i++) {
                        cMap.put(i,Double.valueOf(matchedHotelList.get(i).getDistancesFromUser()));
                    }
                    List<Double> distanceList = new ArrayList<>(cMap.values());
                    Collections.sort(distanceList);
                    List<Hotel> sortedList = new ArrayList<>();
                    for (int i=0; i<distanceList.size();i++) {
                        sortedList.add(matchedHotelList.get(getkey(cMap,distanceList.get(i))));
                    }

                    Intent i = new Intent(GuestHomeScreenActivity.this, ViewMatchedHotelDetailsActivity.class);
                    i.putExtra("hotelList",(Serializable) sortedList);
                    startActivity(i);
                } else {
                    Toast.makeText(GuestHomeScreenActivity.this, "No Matching Hotel Rooms Found.", Toast.LENGTH_SHORT).show();
                }

            }

        } else{
            Toast.makeText(GuestHomeScreenActivity.this, validateInputs()+" is not valid", Toast.LENGTH_SHORT).show();
        }
    }

    public static <K,V> K getkey(Map<K,V> map, V value) {
        for (Map.Entry<K,V> entry : map.entrySet()) {
            if(value.equals(entry.getValue())){
                return entry.getKey();
            }
        } return null;
    }

    private String validateInputs() {
        if(duration.getSelectedItem().toString().contains("No. of Nights")){
            return "Duration";
        } else if(roomType.getSelectedItem().toString().contains("Select a room type")){
            return "RoomType";
        } else if(distance.getSelectedItem().toString().contains("No. of Miles")){
            return "Distance";
        } else {
            return "success";
        }
    }

    private String getLatitudeLongitude(String zip) {
            final Geocoder geocoder = new Geocoder(this);
        String message = null;
        try {
                List<Address> addresses = geocoder.getFromLocationName(zip, 1);
                if (addresses != null && !addresses.isEmpty()) {
                    Address address = addresses.get(0);
                    // Use the address as needed
                    message = String.format("Latitude: %f, Longitude: %f",
                            address.getLatitude(), address.getLongitude());

                } else {
                    // Display appropriate message when Geocoder services are not available
                    Toast.makeText(this, "Unable to geocode zipcode", Toast.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                // handle exception
            }
            return message;
    }

    private List<Hotel> evaluateList(String uzip, List<Hotel> retrievedList) {
        List<Hotel> selectedHotelList = new ArrayList<>();
        Double ulatitude = Double.parseDouble(uzip.split(",")[0].split(":")[1]);
        Double ulongitude = Double.parseDouble(uzip.split(",")[1].split(":")[1]);
        float[] results = new float[1];
        for(int i=0;i<retrievedList.size();i++){
            Hotel h = retrievedList.get(i);
            String hzip = getLatitudeLongitude(h.getHotelZipcode());
            Double hlatitude = Double.parseDouble(hzip.split(",")[0].split(":")[1]);
            Double hlongitude = Double.parseDouble(hzip.split(",")[1].split(":")[1]);
            android.location.Location.distanceBetween(ulatitude, ulongitude, hlatitude, hlongitude, results);
            DecimalFormat df = new DecimalFormat("#.##");
            Double distanceInMiles = Double.parseDouble(df.format(Float.valueOf(results[0]) * (0.000621371) ));

            if( (Double.parseDouble(distance.getSelectedItem().toString()) - distanceInMiles) >= 0.0){

                h.setDistancesFromUser(distanceInMiles.toString());
                h.setDuration(duration.getSelectedItem().toString());
                h.setRoomType(roomType.getSelectedItem().toString());
                h.setCheckinDate(checkInDate.getText().toString());
                selectedHotelList.add(h);
            }
        }

        return selectedHotelList;
    }
}
