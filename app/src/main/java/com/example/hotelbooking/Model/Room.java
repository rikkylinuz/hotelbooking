package com.example.hotelbooking.Model;

import java.io.Serializable;

public class Room implements Serializable {

    private String roomNumber;
    private String hotelName;
    private String roomType;
    private String roomRate;
    private String checkInDate;
    private String roomOccupiedStatus;
    private String roomAvailabilityStatus;

//    Transient Variable
    private String searchedDate;

    public Room(String roomNumber, String hotelName, String roomType, String roomRate, String checkInDate,
                String roomOccupiedStatus, String roomAvailabilityStatus){
        this.roomNumber = roomNumber;
        this.hotelName = hotelName;
        this.roomType = roomType;
        this.roomRate = roomRate;
        this.checkInDate = checkInDate;
        this.roomOccupiedStatus = roomOccupiedStatus;
        this.roomAvailabilityStatus = roomAvailabilityStatus;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomRate() {
        return roomRate;
    }

    public void setRoomRate(String roomRate) {
        this.roomRate = roomRate;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getRoomOccupiedStatus() {
        return roomOccupiedStatus;
    }

    public void setRoomOccupiedStatus(String roomOccupiedStatus) {
        this.roomOccupiedStatus = roomOccupiedStatus;
    }

    public String getRoomAvailabilityStatus() {
        return roomAvailabilityStatus;
    }

    public void setRoomAvailabilityStatus(String roomAvailabilityStatus) {
        this.roomAvailabilityStatus = roomAvailabilityStatus;
    }

    public String getSearchedDate() {
        return searchedDate;
    }

    public void setSearchedDate(String searchedDate) {
        this.searchedDate = searchedDate;
    }
}
