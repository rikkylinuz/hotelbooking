package com.example.hotelbooking.Model;

import java.io.Serializable;

public class Hotel implements Serializable {

    private String hotelName;
    private String hotelZipcode;
    private String availableStandardRooms;
    private String availableDeluxRooms;
    private String availableSuiteRooms;
    private String weekdayStandardPrice;
    private String weekdayDeluxPrice;
    private String weekdaySuitePrice;
    private String hotelAddress;

//  Transient variables
    private String distancesFromUser;
    private String duration;
    private String roomType;
    private String checkinDate;
    private String checkoutDate;
    private String totalPrice;
    private String noOfRoomsForUser;
    private String noOfPeople;

    public Hotel( String hotelname, String hotelzipcode, String availStandardRooms, String availDeluxRooms, String availSuiteRooms,
                  String weekdayStandardPrice, String weekdayDeluxPrice, String weekdaySuitePrice, String hotelAddress){
        this.hotelName = hotelname;
        this.hotelZipcode = hotelzipcode;
        this.availableStandardRooms = availStandardRooms;
        this.availableDeluxRooms = availDeluxRooms;
        this.availableSuiteRooms = availSuiteRooms;
        this.weekdayStandardPrice = weekdayStandardPrice;
        this.weekdayDeluxPrice = weekdayDeluxPrice;
        this.weekdaySuitePrice = weekdaySuitePrice;
        this.hotelAddress = hotelAddress;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelZipcode() {
        return hotelZipcode;
    }

    public void setHotelZipcode(String hotelZipcode) {
        this.hotelZipcode = hotelZipcode;
    }

    public String getAvailableStandardRooms() {
        return availableStandardRooms;
    }

    public void setAvailableStandardRooms(String availableStandardRooms) {
        this.availableStandardRooms = availableStandardRooms;
    }

    public String getAvailableDeluxRooms() {
        return availableDeluxRooms;
    }

    public void setAvailableDeluxRooms(String availableDeluxRooms) {
        this.availableDeluxRooms = availableDeluxRooms;
    }

    public String getAvailableSuiteRooms() {
        return availableSuiteRooms;
    }

    public void setAvailableSuiteRooms(String availableSuiteRooms) {
        this.availableSuiteRooms = availableSuiteRooms;
    }

    public String getWeekdayStandardPrice() {
        return weekdayStandardPrice;
    }

    public void setWeekdayStandardPrice(String weekdayStandardPrice) {
        this.weekdayStandardPrice = weekdayStandardPrice;
    }

    public String getWeekdayDeluxPrice() {
        return weekdayDeluxPrice;
    }

    public void setWeekdayDeluxPrice(String weekdayDeluxPrice) {
        this.weekdayDeluxPrice = weekdayDeluxPrice;
    }

    public String getWeekdaySuitePrice() {
        return weekdaySuitePrice;
    }

    public void setWeekdaySuitePrice(String weekdaySuitePrice) {
        this.weekdaySuitePrice = weekdaySuitePrice;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getDistancesFromUser() {
        return distancesFromUser;
    }

    public void setDistancesFromUser(String distancesFromUser) {
        this.distancesFromUser = distancesFromUser;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getCheckinDate() {
        return checkinDate;
    }

    public void setCheckinDate(String checkinDate) {
        this.checkinDate = checkinDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getNoOfRoomsForUser() {
        return noOfRoomsForUser;
    }

    public void setNoOfRoomsForUser(String noOfRoomsForUser) {
        this.noOfRoomsForUser = noOfRoomsForUser;
    }

    public String getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(String checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public String getNoOfPeople() {
        return noOfPeople;
    }

    public void setNoOfPeople(String noOfPeople) {
        this.noOfPeople = noOfPeople;
    }

}
