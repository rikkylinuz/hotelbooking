package com.example.hotelbooking.Model;

import java.io.Serializable;

public class Reservation implements Serializable {

    private String guestName;
    private String reservationId;
    private String hotelName;
    private String roomType;
    private String numberOfPeople;
    private String numberOfRoom;
    private String checkinDate;
    private String checkoutDate;
    private String payment;
    private String paymentStatus;

    public Reservation() {

    }

    public Reservation(String guestName, String reservationId, String hotelName, String roomType, String numberOfPeople, String numberOfRoom,
                       String checkinDate, String checkoutDate, String payment, String paymentStatus){
        this.guestName = guestName;
        this.reservationId = reservationId;
        this.hotelName = hotelName;
        this.roomType = roomType;
        this.numberOfPeople = numberOfPeople;
        this.numberOfRoom = numberOfRoom;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
        this.payment = payment;
        this.paymentStatus = paymentStatus;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(String numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public String getNumberOfRoom() {
        return numberOfRoom;
    }

    public void setNumberOfRoom(String numberOfRoom) {
        this.numberOfRoom = numberOfRoom;
    }

    public String getCheckinDate() {
        return checkinDate;
    }

    public void setCheckinDate(String checkinDate) {
        this.checkinDate = checkinDate;
    }

    public String getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(String checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
