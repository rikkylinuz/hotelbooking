package com.example.hotelbooking.Model;

import java.io.Serializable;

public class User implements Serializable {

    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String role;
    private String phone;
    private String email;
    private String streetNo;
    private String city;
    private String state;
    private String zipcode;
    private String gender;
    private String country;
    private String creditCardNumber;
    private String creditCardExpiry;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String newUserName, String newUserPassword, String newUserFirstname, String newUserLastname, String newUserRole, String newUserPhone, String newUserEmail, String newUserStreetNo, String newUserCity, String newUserState, String newUserZipcode, String newUserGender, String newUserCountry) {
    }


    public User(String username, String password, String firstname, String lastname,
                String role, String phone, String email, String streetNo, String city,
                String state, String zipcode, String gender, String country,
                String creditCardNumber, String creditCardExpiry) {
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.role = role;
        this.phone = phone;
        this.email = email;
        this.streetNo = streetNo;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.gender = gender;
        this.country = country;
        this.creditCardNumber = creditCardNumber;
        this.creditCardExpiry = creditCardExpiry;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardExpiry() {
        return creditCardExpiry;
    }

    public void setCreditCardExpiry(String creditCardExpiry) {
        this.creditCardExpiry = creditCardExpiry;
    }
}
