package com.example.hotelbooking.Adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Activities.ViewSelectedHotelDetailsActivity;
import com.example.hotelbooking.Model.Hotel;
import com.example.hotelbooking.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MatchedHotelsRecyclerAdapter extends RecyclerView.Adapter<MatchedHotelsRecyclerAdapter.ViewHolder>{


    private Context context;
    private List<Hotel> hotelsList;
    private LayoutInflater layoutInflater;
    private String selectedRoomType;
    private String checkinDate;
    private String noOfNights;

    public MatchedHotelsRecyclerAdapter(Context context, List<Hotel> hotelList){
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.hotelsList = hotelList;
        this.selectedRoomType = hotelList.get(0).getRoomType();
        this.checkinDate = hotelList.get(0).getCheckinDate();
        this.noOfNights = hotelList.get(0).getDuration();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.matched_hotel_list_adapter_row_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchedHotelsRecyclerAdapter.ViewHolder holder, int position) {
        DecimalFormat df = new DecimalFormat("#.##");

        Hotel hotel = hotelsList.get(position);

        holder.hotelName.setText(hotel.getHotelName());
        holder.roomType.setText(hotel.getRoomType());
        holder.distance.setText(hotel.getDistancesFromUser()+"miles");
        holder.duration.setText(hotel.getDuration()+"nights");

        // Logic to parse TextView to date and Find checkout date from duration(No of Nights)
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Date date = null;
        try {
            date = sdf.parse(checkinDate); // parses the string date to get a date object
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        c.add(Calendar.DATE, Integer.parseInt(noOfNights));
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        String coutDate = dateFormat.format(c.getTime());
        Date checkoutDate =null;
        try {
            checkoutDate = sdf.parse(coutDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        DateFormat dateFormatDB = new SimpleDateFormat("yyyy-MM-dd");
        hotel.setCheckoutDate(coutDate);

        //Logic to Calculate No of weekdays and weekends in the duration stay
        Integer weekdays = 0;
        Integer weekends = 0;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date);
        cal2.setTime(checkoutDate);
        while (cal1.before(cal2)) {
            if ((Calendar.SATURDAY == cal1.get(Calendar.DAY_OF_WEEK))
                    || (Calendar.SUNDAY == cal1.get(Calendar.DAY_OF_WEEK))) {
                weekends++;
            } else{
                weekdays++;
            }
            cal1.add(Calendar.DATE,1);
        }
//        hotel.setCheckinDate(dateFormatDB.format(cal1.getTime()));

        if(selectedRoomType.equalsIgnoreCase("standard")) {
            holder.noOfRooms.setText(hotel.getAvailableStandardRooms());

            Integer weekdayPrice = Integer.parseInt(hotel.getWeekdayStandardPrice());
            Integer totalPrice = (weekdayPrice * weekdays) + ((weekdayPrice+50) * weekends);
            Double priceWithTax = Double.parseDouble(df.format(totalPrice + (totalPrice * 0.0875) ));
            holder.roomRate.setText("$."+priceWithTax.toString());
            hotel.setTotalPrice(priceWithTax.toString());
        } else if(selectedRoomType.equalsIgnoreCase("delux")) {
            holder.noOfRooms.setText(hotel.getAvailableDeluxRooms());

            Integer weekdayPrice = Integer.parseInt(hotel.getWeekdayDeluxPrice());
            Integer totalPrice = (weekdayPrice * weekdays) + ((weekdayPrice+50) * weekends);
            Double priceWithTax = Double.parseDouble(df.format(totalPrice + (totalPrice * 0.0875) ) );
            holder.roomRate.setText("$."+priceWithTax.toString());
            hotel.setTotalPrice(priceWithTax.toString());
        } else if(selectedRoomType.equalsIgnoreCase("suite")) {
            holder.noOfRooms.setText(hotel.getAvailableSuiteRooms());

            Integer weekdayPrice = Integer.parseInt(hotel.getWeekdaySuitePrice());
            Integer totalPrice = (weekdayPrice * weekdays) + ((weekdayPrice+50) * weekends);
            Double priceWithTax = Double.parseDouble(df.format(totalPrice + (totalPrice * 0.0875) ));
            holder.roomRate.setText("$."+priceWithTax.toString());
            hotel.setTotalPrice(priceWithTax.toString());
        }

    }

    @Override
    public int getItemCount() {
        return hotelsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView hotelName;
        public TextView noOfRooms;
        public TextView roomType;
        public TextView roomRate;
        public TextView duration;
        public TextView distance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.hotelName = itemView.findViewById(R.id.textHotelName);
            this.roomType = itemView.findViewById(R.id.textRoomType);
            this.noOfRooms = itemView.findViewById(R.id.textNoOfRooms);
            this.roomRate = itemView.findViewById(R.id.textRoomRate);
            this.duration = itemView.findViewById(R.id.textDuration);
            this.distance = itemView.findViewById(R.id.textDistance);
        }

        @Override
        public void onClick(View view) {
            final Intent intent;
            intent = new Intent( context, ViewSelectedHotelDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("hotel", hotelsList.get(getLayoutPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
