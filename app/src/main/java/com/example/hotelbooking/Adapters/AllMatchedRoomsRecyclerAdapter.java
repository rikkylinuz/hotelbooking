package com.example.hotelbooking.Adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Activities.ViewSelectedRoomActivity;
import com.example.hotelbooking.Model.Room;
import com.example.hotelbooking.R;

import java.util.List;

public class AllMatchedRoomsRecyclerAdapter extends RecyclerView.Adapter<AllMatchedRoomsRecyclerAdapter.ViewHolder>{


    private Context context;
    private List<Room> roomsList;
    private LayoutInflater layoutInflater;
    private String date;

    public AllMatchedRoomsRecyclerAdapter(Context context, List<Room> roomList, String date){
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.roomsList = roomList;
        this.date = date;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.all_matched_rooms_list_adapter_row_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllMatchedRoomsRecyclerAdapter.ViewHolder holder, int position) {
        Room room = roomsList.get(position);

        holder.hotelName.setText(room.getHotelName());
        holder.roomType.setText(room.getRoomType());
        holder.roomNumber.setText(room.getRoomNumber());
        holder.startDate.setText(date);

        room.setSearchedDate(date);
    }

    @Override
    public int getItemCount() {
        return roomsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView hotelName;
        public TextView startDate;
        public TextView roomType;
        public TextView roomNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.hotelName = itemView.findViewById(R.id.hotelName);
            this.roomType = itemView.findViewById(R.id.roomType);
            this.roomNumber = itemView.findViewById(R.id.roomNumber);
            this.startDate = itemView.findViewById(R.id.startDate);
        }

        @Override
        public void onClick(View view) {
            final Intent intent;
            intent = new Intent( context, ViewSelectedRoomActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("selectedroom", roomsList.get(getLayoutPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
