package com.example.hotelbooking.Adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Activities.ViewSelectedUserActivity;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.R;

import java.util.List;

public class UsersRecyclerAdapter extends RecyclerView.Adapter<UsersRecyclerAdapter.ViewHolder>{


    private Context context;
    private List<User> userList;
    private LayoutInflater layoutInflater;

    public UsersRecyclerAdapter(Context context, List userList){
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.userList = userList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.userlist_adapter_row_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersRecyclerAdapter.ViewHolder holder, int position) {
        User user = userList.get(position);

        holder.firstName.setText(user.getFirstname());
        holder.lastName.setText(user.getLastname());
        holder.role.setText(user.getRole());
        holder.phone.setText(user.getPhone());

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView firstName;
        public TextView lastName;
        public TextView role;
        public TextView phone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.firstName = itemView.findViewById(R.id.textFirstName);
            this.lastName = itemView.findViewById(R.id.textLastName);
            this.role = itemView.findViewById(R.id.textRole);
            this.phone = itemView.findViewById(R.id.textPhone);
        }

        @Override
        public void onClick(View view) {
            final Intent intent;
            intent = new Intent( context, ViewSelectedUserActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("user", userList.get(getLayoutPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
