package com.example.hotelbooking.Adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hotelbooking.Activities.ViewSelectedReservationManagerActivity;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.R;

import java.util.List;

public class AllReservationsManagerRecyclerAdapter extends RecyclerView.Adapter<AllReservationsManagerRecyclerAdapter.ViewHolder>{


    private Context context;
    private List<Reservation> reservationsList;
    private LayoutInflater layoutInflater;

    public AllReservationsManagerRecyclerAdapter(Context context, List<Reservation> reservationList){
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.reservationsList = reservationList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.all_reservations_list_adapter_row_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllReservationsManagerRecyclerAdapter.ViewHolder holder, int position) {
        Reservation reservation = reservationsList.get(position);

        holder.reservationId.setText(reservation.getReservationId());
        holder.hotelName.setText(reservation.getHotelName());
        holder.roomType.setText(reservation.getRoomType());
        holder.checkinDate.setText(reservation.getCheckinDate());
        holder.checkoutDate.setText(reservation.getCheckoutDate());

    }

    @Override
    public int getItemCount() {
        return reservationsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView reservationId;
        public TextView hotelName;
        public TextView roomType;
        public TextView checkinDate;
        public TextView checkoutDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.reservationId = itemView.findViewById(R.id.textReservationId);
            this.hotelName = itemView.findViewById(R.id.textHotelName);
            this.roomType = itemView.findViewById(R.id.textRoomType);
            this.checkinDate = itemView.findViewById(R.id.textCheckinDate);
            this.checkoutDate = itemView.findViewById(R.id.textCheckoutDate);
        }

        @Override
        public void onClick(View view) {
            final Intent intent;
            intent = new Intent( context, ViewSelectedReservationManagerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("reservation", reservationsList.get(getLayoutPosition()));
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
