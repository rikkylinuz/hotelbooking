package com.example.hotelbooking.Util;

public class Constants {


    public static final String TABLE_USER = "user";
    //USER_TABLE details
    public static final String USER_USER_NAME = "username";
    public static final String USER_PASSWORD = "password";
    public static final String USER_LAST_NAME = "lastname";
    public static final String USER_FIRST_NAME = "firstname";
    public static final String USER_ROLE =  "role";
    public static final String USER_PHONE = "phone";
    public static final String USER_EMAIL = "email";
    public static final String USER_STREET_NO = "street_no";
    public static final String USER_CITY = "city";
    public static final String USER_STATE = "state";
    public static final String USER_ZIP_CODE = "zip_code";
    public static final String USER_GENDER = "gender";
    public static final String USER_COUNTRY = "country";
    public static final String USER_CC_NUMBER = "ccnumber";
    public static final String USER_CC_EXPIRY = "ccexpiry";

    public static final String GUEST_ROLE =  "guest";


    public static final String TABLE_RESERVATION = "reservation";
    // RESERVATION details
    public static final String RESERVATION_GUESTNAME = "guestname";
    public static final String RESERVATION_RESERVATIONID = "reservation_id";
    public static final String RESERVATION_HOTELNAME = "hotelname";
    public static final String RESERVATION_ROOMTYPE = "room_type";
    public static final String RESERVATION_NUMBER_OF_PEOPLE = "num_people";
    public static final String RESERVATION_NUMBER_OF_ROOM = "num_room";
    public static final String RESERVATION_CHECKIN_DATE = "checkin_date";
    public static final String RESERVATION_CHECKOUT_DATE = "checkout_date";
    public static final String RESERVATION_PAYMENT = "payment";
    public static final String RESERVATION_PAYMENT_STATUS = "payment_status";


    public static final String TABLE_ROOM = "room";
    // RESERVATION details
    public static final String ROOM_ROOMNUMBER = "room_number";
    public static final String ROOM_HOTELNAME = "hotel_name";
    public static final String ROOM_ROOMTYPE = "room_type";
    public static final String ROOM_ROOMRATE = "room_rate";
    public static final String ROOM_ROOM_OCCUPIED_STATUS = "room_occupied_status";
    public static final String ROOM_CHECKIN_DATE = "checkin_date";
    public static final String ROOM_ROOM_AVAILABLE_STATUS = "room_availability_status";
}
