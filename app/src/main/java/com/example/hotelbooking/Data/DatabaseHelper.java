package com.example.hotelbooking.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.hotelbooking.Model.Hotel;
import com.example.hotelbooking.Model.Reservation;
import com.example.hotelbooking.Model.Room;
import com.example.hotelbooking.Model.User;
import com.example.hotelbooking.Util.Constants;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper {

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static DatabaseHelper instance;
    Cursor c = null;
    private static final String DB_NAME = "HotelManagementDB.db";
    private static final int DB_VERSION = 5;

    private DatabaseHelper(@Nullable Context context) {
        this.openHelper = new DatabaseAssetHelper(context);
    }

    public static DatabaseHelper getInstance(Context context){
        if(instance == null){
            instance= new DatabaseHelper(context);
        }
        return instance;
    }

    public void open(){
        this.db = openHelper.getWritableDatabase();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public User getUser(String userName, String password){
        User user=null;
        Cursor cursor = this.db.query(Constants.TABLE_USER,new String[] {Constants.USER_USER_NAME,Constants.USER_PASSWORD,Constants.USER_FIRST_NAME,
                        Constants.USER_LAST_NAME,Constants.USER_ROLE,Constants.USER_PHONE,Constants.USER_EMAIL,Constants.USER_STREET_NO,Constants.USER_CITY,
                        Constants.USER_STATE,Constants.USER_ZIP_CODE,Constants.USER_GENDER, Constants.USER_COUNTRY, Constants.USER_CC_NUMBER,Constants.USER_CC_EXPIRY},
                Constants.USER_USER_NAME+"=? AND "+Constants.USER_PASSWORD+"=?",
                new String[] {userName, password},null,null,null);

        if(cursor.getCount()!=0) {
            cursor.moveToFirst();
            user = new User(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                    cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),
                    cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getString(11),
                    cursor.getString(12),cursor.getString(13),cursor.getString(14));

        }
        cursor.close();
        return user;
    }

    //Register user
    public void addUser(User user){

        ContentValues value = new ContentValues();
        value.put(Constants.USER_USER_NAME,user.getUsername());
        value.put(Constants.USER_PASSWORD,user.getPassword());
        value.put(Constants.USER_FIRST_NAME,user.getFirstname());
        value.put(Constants.USER_LAST_NAME,user.getLastname());
        value.put(Constants.USER_ROLE, user.getRole());
        value.put(Constants.USER_PHONE,user.getPhone());
        value.put(Constants.USER_EMAIL,user.getEmail());
        value.put(Constants.USER_STREET_NO,user.getStreetNo());
        value.put(Constants.USER_CITY,user.getCity());
        value.put(Constants.USER_STATE,user.getState());
        value.put(Constants.USER_ZIP_CODE,user.getZipcode());
        value.put(Constants.USER_GENDER,user.getGender());
        value.put(Constants.USER_COUNTRY,user.getCountry());
        value.put(Constants.USER_CC_NUMBER,user.getCreditCardNumber());
        value.put(Constants.USER_CC_EXPIRY,user.getCreditCardExpiry());

        db.insert(Constants.TABLE_USER,null,value);
    }

    public List<User> getUserBasedOnLastName(String userLastName){

        List<User> userList= new ArrayList<>();
        Cursor cursor = this.db.query(Constants.TABLE_USER,new String[] {Constants.USER_USER_NAME,Constants.USER_PASSWORD,Constants.USER_FIRST_NAME,
                        Constants.USER_LAST_NAME,Constants.USER_ROLE,Constants.USER_PHONE,Constants.USER_EMAIL,Constants.USER_STREET_NO,Constants.USER_CITY,
                        Constants.USER_STATE,Constants.USER_ZIP_CODE,Constants.USER_GENDER, Constants.USER_COUNTRY, Constants.USER_CC_NUMBER,Constants.USER_CC_EXPIRY},
                Constants.USER_LAST_NAME+"=?",
                new String[] {userLastName},null,null,null);

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                User user = new User(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),
                        cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getString(11),
                        cursor.getString(12),cursor.getString(13),cursor.getString(14));
                userList.add(user);
            }

        }
        cursor.close();
        return userList;
    }

    public int updateUser(String uname, String firstname, String lastname, String role, String phone,
                           String email, String streetNo, String city, String state,String zipcode){

        ContentValues cv = new ContentValues();
        cv.put("firstname",firstname);
        cv.put("lastname",lastname);
        cv.put("role",role);
        cv.put("phone",phone);
        cv.put("email",email);
        cv.put("street_no",streetNo);
        cv.put("city",city);
        cv.put("state",state);
        cv.put("zip_code",zipcode);

        int output = this.db.update("user",cv, "username=?", new String[] { uname });

        return output;

    }

    public int deleteUser(String uname) {
        int output = this.db.delete("user","username=?", new String[] { uname });
        return output;
    }

    public List<User> getAllUser(){

        List<User> userList= new ArrayList<>();
        Cursor cursor = this.db.rawQuery("select * from user", null);

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                User user = new User(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),
                        cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getString(11),
                        cursor.getString(12),cursor.getString(13),cursor.getString(14));
                userList.add(user);
            }

        }
        cursor.close();
        return userList;
    }

    public List<Reservation> getReservations(String userName, String date){
        List<Reservation> reservationList = new ArrayList<>();
        String whereStatements = Constants.RESERVATION_GUESTNAME+"=? "+" and "+Constants.RESERVATION_CHECKIN_DATE+">=?";

        Cursor cursor = this.db.query(Constants.TABLE_RESERVATION,new String[] {Constants.RESERVATION_GUESTNAME,Constants.RESERVATION_RESERVATIONID,Constants.RESERVATION_HOTELNAME,Constants.RESERVATION_ROOMTYPE,Constants.RESERVATION_NUMBER_OF_PEOPLE,Constants.RESERVATION_NUMBER_OF_ROOM,Constants.RESERVATION_CHECKIN_DATE,Constants.RESERVATION_CHECKOUT_DATE,Constants.RESERVATION_PAYMENT,
                        Constants.RESERVATION_PAYMENT_STATUS},
                whereStatements,
                new String[] {userName, date},null,null,null);

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                Reservation reservation = new Reservation(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9));
                reservationList.add(reservation);
            }

        }
        cursor.close();
        return reservationList;
    }

    public User getUserBasedOnUserName(String userName){

        User user = null;
        Cursor cursor = this.db.query(Constants.TABLE_USER,new String[] {Constants.USER_USER_NAME,Constants.USER_PASSWORD,Constants.USER_FIRST_NAME,
                        Constants.USER_LAST_NAME,Constants.USER_ROLE,Constants.USER_PHONE,Constants.USER_EMAIL,Constants.USER_STREET_NO,Constants.USER_CITY,
                        Constants.USER_STATE,Constants.USER_ZIP_CODE,Constants.USER_GENDER, Constants.USER_COUNTRY, Constants.USER_CC_NUMBER,Constants.USER_CC_EXPIRY},
                Constants.USER_USER_NAME+"=?",
                new String[] {userName},null,null,null);

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                user = new User(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),
                        cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getString(11),
                        cursor.getString(12),cursor.getString(13),cursor.getString(14));
            }

        }
        cursor.close();
        return user;
    }

    public int updateUserProfile(String uname, String  passwordStr, String  firstnameStr, String lastnameStr,
                                 String phoneStr, String  emailStr, String  streetNoStr, String cityStr, String stateStr, String zipcodeStr, String genderStr,
                                 String countryStr, String roleStr, String ccNumberStr, String ccExpiryStr){

        ContentValues cv = new ContentValues();
        cv.put("firstname",firstnameStr);
        cv.put("password", passwordStr);
        cv.put("lastname",lastnameStr);
        cv.put("role",roleStr);
        cv.put("phone",phoneStr);
        cv.put("email",emailStr);
        cv.put("street_no",streetNoStr);
        cv.put("city",cityStr);
        cv.put("state",stateStr);
        cv.put("zip_code",zipcodeStr);
        cv.put("gender",genderStr);
        cv.put("country", countryStr);
        cv.put("ccnumber",ccNumberStr);
        cv.put("ccexpiry", ccExpiryStr);

        int output = this.db.update("user",cv, "username=?", new String[] { uname });

        return output;
    }

    public int updateReservation(String reservationIdStr, String roomTypeStr, String checkInDateStr, String checkOutDateStr,
                                 String noOfRoomsStr, String noOfPeopleStr){

        ContentValues cv = new ContentValues();
        cv.put("room_type",roomTypeStr);
        cv.put("num_people",noOfPeopleStr);
        cv.put("num_room",noOfRoomsStr);
        cv.put("checkin_date",checkInDateStr);
        cv.put("checkout_date",checkOutDateStr);

        int output = this.db.update("reservation",cv, "reservation_id=?", new String[] { reservationIdStr });

        return output;

    }

    public int deleteReservation(String reservationId) {
        int output = this.db.delete("reservation","reservation_id=?", new String[] { reservationId });
        return output;
    }

    public List<Reservation> getAllReservations(String date, String hotelName){

        List<Reservation> reservationList= new ArrayList<>();
        Cursor cursor = this.db.rawQuery("select * from reservation where hotelname = ? and checkin_date >= ?", new String[]{hotelName, date});

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                Reservation reservation = new Reservation(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),
                        cursor.getString(8),cursor.getString(9));
                reservationList.add(reservation);
            }

        }
        cursor.close();
        return reservationList;
    }

    public List<Hotel> getHotelDetails(String minPrice, String maxPrice, String roomType){
        List<Hotel> outputList = new ArrayList<>();
        Cursor cursor = null;
        if(roomType.equalsIgnoreCase("standard")) {
            cursor = this.db.rawQuery("select * from hotel" +
                    " where available_standard_rooms > 0 and weekday_standard_price > "+minPrice+ " and weekday_standard_price < "+maxPrice+";", null);
        } else if(roomType.equalsIgnoreCase("delux")) {
            cursor = this.db.rawQuery("select * from hotel" +
                    " where available_delux_rooms > 0 and weekday_delux_price > "+minPrice+ " and weekday_delux_price < "+maxPrice+";", null);
        } else if(roomType.equalsIgnoreCase("suite")) {
            cursor = this.db.rawQuery("select * from hotel" +
                    " where available_suite_rooms > 0 and weekday_suite_price > "+minPrice+ " and weekday_suite_price < "+maxPrice+";", null);
        }

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                Hotel hotel = new Hotel(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7),cursor.getString(8));
                outputList.add(hotel);
            }

        }
        cursor.close();
        return outputList;
    }

    public long storeReservation(Reservation r) {
        ContentValues cv = new ContentValues();
        cv.put(Constants.RESERVATION_GUESTNAME, r.getGuestName());
        cv.put(Constants.RESERVATION_HOTELNAME, r.getHotelName());
        cv.put(Constants.RESERVATION_ROOMTYPE, r.getRoomType());
        cv.put(Constants.RESERVATION_NUMBER_OF_PEOPLE, r.getNumberOfPeople());
        cv.put(Constants.RESERVATION_NUMBER_OF_ROOM, r.getNumberOfRoom());
        cv.put(Constants.RESERVATION_CHECKIN_DATE, r.getCheckinDate());
        cv.put(Constants.RESERVATION_CHECKOUT_DATE, r.getCheckoutDate());
        cv.put(Constants.RESERVATION_PAYMENT, r.getPayment());
        cv.put(Constants.RESERVATION_PAYMENT_STATUS, r.getPaymentStatus());

        long output = db.insert(Constants.TABLE_RESERVATION,null,cv);
        System.out.println("OUTPUT:: "+output);
        return output;
    }

    public void updateNumberOfRooms(Hotel hotel) {

        if(hotel.getRoomType().equalsIgnoreCase("standard")){System.out.println("inside standard no of rooms");
            String updateQuery = "UPDATE hotel SET available_standard_rooms = available_standard_rooms - ? WHERE hotel_name = ?;";
            db.execSQL(updateQuery,new String[]{hotel.getNoOfRoomsForUser(), hotel.getHotelName().toLowerCase()});
        } else if(hotel.getRoomType().equalsIgnoreCase("delux")){
            String updateQuery = "UPDATE hotel SET available_delux_rooms = available_delux_rooms - ? WHERE hotel_name = ?;";
            db.execSQL(updateQuery,new String[]{hotel.getNoOfRoomsForUser(), hotel.getHotelName().toLowerCase()});
        } else if(hotel.getRoomType().equalsIgnoreCase("suite")){
            String updateQuery = "UPDATE hotel SET available_suite_rooms = available_suite_rooms - ? WHERE hotel_name = ?;";
            db.execSQL(updateQuery,new String[]{hotel.getNoOfRoomsForUser(), hotel.getHotelName().toLowerCase()});
        }

    }

    public void updateRoomStatus(Hotel hotel) {
        String updateQuery = "\n" +
                "UPDATE room SET checkin_date = ? , room_occupied_status = ?, room_availability_status = ? \n" +
                "WHERE room_number in (SELECT room_number FROM room where hotel_name = ? AND room_type = ? AND room_availability_status = 'yes' AND room_occupied_status = 'not-occupied' LIMIT ?);";
        db.execSQL(updateQuery,new String[]{hotel.getCheckinDate(), "occupied", "no", hotel.getHotelName().toLowerCase(), hotel.getRoomType().toLowerCase(), hotel.getNoOfRoomsForUser()});

    }

    public List<Room> getAvailableRoomsOnHotelName(String hotelName){

        List<Room> roomList= new ArrayList<>();
        String whereStatements = Constants.ROOM_HOTELNAME+"=? "+" and "+Constants.ROOM_ROOM_OCCUPIED_STATUS+"=? and "+Constants.ROOM_ROOM_AVAILABLE_STATUS+"=?";
        Cursor cursor = this.db.query(Constants.TABLE_ROOM,new String[] {Constants.ROOM_ROOMNUMBER, Constants.ROOM_HOTELNAME,
                Constants.ROOM_ROOMTYPE, Constants.ROOM_ROOMRATE, Constants.ROOM_CHECKIN_DATE,
                Constants.ROOM_ROOM_OCCUPIED_STATUS, Constants.ROOM_ROOM_AVAILABLE_STATUS},
                whereStatements,
                new String[] {hotelName, "not-occupied", "yes"},null,null,null);

        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                Room room = new Room(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6));
                roomList.add(room);
            }

        }
        cursor.close();
        return roomList;
    }

    public int updateRoom(String roomType, String roomRate, String roomOccupiedStatus, String roomAvailableStatus, String roomNumber){

        ContentValues cv = new ContentValues();
        cv.put("room_type",roomType);
        cv.put("room_rate",roomRate);
        cv.put("room_occupied_status",roomOccupiedStatus);
        cv.put("room_availability_status",roomAvailableStatus);

        int output = this.db.update("room",cv, "room_number=?", new String[] { roomNumber });
        return output;
    }

    public Room getRoomDetailsBasedOnRoomNumber(String roomNumber) {
        Room room = null;
        Cursor cursor = null;
        cursor = db.rawQuery("select * from room" +
                " where room_number = ? ;", new String[] {roomNumber});
        if(cursor.getCount()!=0) {
            while (cursor.moveToNext()) {
                room = new Room(cursor.getString(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),
                        cursor.getString(4),cursor.getString(5),cursor.getString(6));
            }

        }
        cursor.close();
        return room;
    }
}